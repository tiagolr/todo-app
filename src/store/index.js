import uuid from 'uuid'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts: JSON.parse(window.localStorage.getItem('posts') || '{}')
  },
  actions: {
    createPost (ctx, text) {
      ctx.commit('updatePost', {
        id: uuid(),
        text,
        checked: false,
        created: Date.now()
      })
    }
  },
  mutations: {
    updatePost (state, post) {
      Vue.set(state.posts, post.id, post)
      window.localStorage.setItem('posts', JSON.stringify(state.posts))
    },
    deletePost (state, post) {
      Vue.delete(state.posts, post.id)
      window.localStorage.setItem('posts', JSON.stringify(state.posts))
    }
  }
})
